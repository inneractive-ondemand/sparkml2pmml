name := "convert"

version := "1.0"

scalaVersion := "2.11.8"


libraryDependencies ++= Seq(
  "org.jpmml" % "jpmml-sparkml" % "1.3.3" exclude("log4j", "log4j"),
  "org.jpmml" % "pmml-evaluator" % "1.3.0" exclude("log4j", "log4j"),
  "com.github.scopt" %% "scopt" % "3.5.0" exclude("log4j", "log4j"),
  "ch.qos.logback" % "logback-classic" % "1.1.7",
  "log4j" % "log4j" % "1.2.17",
  "commons-lang" % "commons-lang" % "2.6",
  "com.fasterxml.jackson.core" % "jackson-core" % "2.9.2",
  "com.fasterxml.jackson.core" % "jackson-databind" % "2.9.2",
  "com.fasterxml.jackson.module" % "jackson-module-scala_2.11" % "2.9.2",
  "org.apache.hadoop" % "hadoop-client" % "2.7.2",
  "org.apache.spark" %% "spark-mllib" % "2.2.1" exclude("log4j", "log4j"),
  "com.typesafe" % "config" % "1.2.1"


)



lazy val root =  Project(
  id = "sparkml2pmml",
  base = file(".")
)
