package com.inneractive.convert

import java.io.{File, FileOutputStream}
import javax.xml.transform.stream.StreamResult

import com.typesafe.config.{Config, ConfigFactory}
import org.apache.spark.ml.PipelineModel
import org.apache.spark.sql.types._
import org.apache.spark.{SparkConf, SparkContext}
import org.dmg.pmml.PMML
import org.slf4j.{Logger, LoggerFactory}


object MLToPMMLConverter extends App{

  implicit val config:Config = ConfigFactory.load()


  val schemaFilterFeatures = new StructType(Array(
    StructField("age", DoubleType, nullable = true),
    StructField("workclass", StringType, nullable = true),
    StructField("fnlwgt", DoubleType, nullable = true),
    StructField("education", StringType, nullable = true),
    StructField("education_num", DoubleType, nullable = true),
    StructField("marital_status", StringType, nullable = true),
    StructField("occupation", StringType, nullable = true),
    StructField("relationship", StringType, nullable = true),
    StructField("race", StringType, nullable = true),
    StructField("sex", StringType, nullable = true),
    StructField("capital_gain", DoubleType, nullable = true),
    StructField("capital_loss", DoubleType, nullable = true),
    StructField("hours_per_week", DoubleType, nullable = true),
    StructField("native_country", StringType, nullable = true),
    StructField("income", StringType, nullable = true)
  ))

  implicit val log = LoggerFactory.getLogger(this.getClass)


  val conf: SparkConf = new SparkConf().setAppName("cerberus.spark.appname").setMaster("local[*]")
  implicit val sc = new SparkContext(conf)

  val localBaseLocation: String = "savedModel"

  log.info(s"Start Spark engine to load from $localBaseLocation")
  val sparkModel: PipelineModel = PipelineModel.load(s"$localBaseLocation")

  val pmmlModelLocation = new File(s"covertedModel/PMML")
  if (!pmmlModelLocation.exists()) pmmlModelLocation.mkdirs()
  val pmmlLocalFile = new File(pmmlModelLocation, "PMML.xml")

  log.info(s"Write PMML file at $pmmlLocalFile")

  val pmmModel: PMML = org.jpmml.sparkml.ConverterUtil.toPMML(schemaFilterFeatures, sparkModel)
  val modelOutputStream = new FileOutputStream(pmmlLocalFile)
  org.jpmml.model.JAXBUtil.marshalPMML(pmmModel, new StreamResult(modelOutputStream))
  modelOutputStream.flush()
  modelOutputStream.close()
}
